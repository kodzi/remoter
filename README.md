Basic remote controller using Tornado and cec-client wrapper

##Currently##
* basic remote calls such as changing AVR inputs, volume and power
* portforwarding required if want to access from outside. port used 8080


###TODO:###
* TV control
* IR-capabilities: IR for remote (wider device support) and CEC for status reading

##Requirements:##
* check requirements.txt for components
* libcec-python
* devices with CEC support

##Setup##
Raspberry Pi -----> AVR -------> TV

##Screenshots##
*check source folder