from tornado import httpserver, web, ioloop, gen, escape
from tornado.options import define, options
import os
import cec

define ("port", default=8080, help="run on given port", type=int)
define ("password", default="admin", help="password changed")
icon_path = './media/icons'
inputs = ("Blu-ray", "VCR","CBL","GAME","AUX","AM","FM","TV","USB", "tvpower")
class BaseHandler(web.RequestHandler):
	def check_permissions(self, username, password):
		if username == 'default' and password == options.password:
			return True
		return False
	def set_current_user(self, user):
		if user:
			print("Set cookie for " + user)
			self.set_secure_cookie('user', escape.json_encode(user))
		else:
			self.clear_cookie('user')

	def get_current_user(self):
		return self.get_secure_cookie('user')


class LoginHandler(BaseHandler):
	def get(self):
		print(self.request.remote_ip+": joined")
		if not self.current_user:
			self.render('templates/login.html')
			return
		self.render('templates/index.html')

	def post(self):
		print("Auth user")
		username = self.get_argument('username')
		password = self.get_argument('password')
		auth = self.check_permissions(username,password)
		if auth:
			self.set_current_user(username)
			self.render('templates/index.html')
		else:
			self.redirect('templates/login.html')

class LogoutHandler(BaseHandler):
		def get(self):
			self.clear_cookie('user')
			self.redirect('/')


class RemHandler(BaseHandler):
	def get(self, input):
		print(self.current_user+"("+self.request.remote_ip+"): called "+input)

		if not self.current_user:
			self.redirect('/')
			return
		cec.execute(input)

if __name__ == "__main__":
	app = web.Application(handlers=[(r"/", LoginHandler),
									(r"/logout", LogoutHandler),
									(r"/init/(\w+)", RemHandler),
									(r"/js/(.*)", web.StaticFileHandler, {'path': './templates/js'}),
									(r"/img/(.*)", web.StaticFileHandler, {'path': './templates/img'}),
									(r"/css/(.*)", web.StaticFileHandler, {'path': './templates/css'}),
									],
									cookie_secret="p82348023urjwn2elhtui23h5it3m2fipj23pt",
									debug=True)
	options.parse_command_line()	
	server = httpserver.HTTPServer(app)
	server.listen(options.port)

	print("Server started")
	ioloop.IOLoop.instance().start()
