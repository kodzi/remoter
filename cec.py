#!/usr/bin/env python
import subprocess
import sys

avr = { "end": "45", "setmenu": "0A", "exit" : "0D", "enter": "2B",
	'up' : '01', 'left'   : '02', 'right': '03', 'soundfield' : '33',
	'input': '34','osd'   : '35',
	'volup': '41','voldown': '42',
	'mute' : '43','muteon': '65' ,'muteoff' : '66',
	'dvd': '69 01' ,'vcr': '69 02',
	'cbl': '69 03' ,'game': '69 04',
	'aux': '69 05' ,'am': '69 06',
	'tuner': '69 07' ,'tv': '69 08',
	'usb': '69 09',
	'onInput1': '6A 01' ,'onInput2': '6A 02',
        'onInput3': '6A 03' ,'onInput4': '6A 04',
        'onInput5': '6A 05' ,'onInput6': '6A 06',
        'onInput7': '6A 07' ,'onInput8': '6A 08','onInput9': '6A 09',
	'power'	: '6B', 'poweroff' : '6C', 'poweron' : '6D',
	'tvpower' : 'on 0' }

inputs = ("Blu-ray", "VCR","CBL","GAME","AUX","AM","FM","TV","USB")	
def execute(command):
	com = ""
	if command[0:2] == 'tv':
		com = avr[command]
	else:
		com = 'tx 15 44 ' + avr[command]
	#setup echo command and direct the output to a pipe
	p1 = subprocess.Popen(['echo', com], stdout = subprocess.PIPE)
	#send p1's output to p2
	p2 = subprocess.Popen(['cec-client', '-s', '-d', '1'], stdin=p1.stdout)
	p1.stdout.close() #closing p1's output so p2's don't need to wait for more
	
	output = p2.communicate()[0] #run piped commands

def main():
	#command_line = raw_input()
	#args = shlex.split(command_line)
	cmd = sys.argv[1]
	if cmd  == "cmd":
		print avr.keys()
	elif cmd == "inputs":
		for x in inputs:
			print x
	else:
		print str(cmd)
		execute(cmd)

if __name__ == "__main__":
	main()
